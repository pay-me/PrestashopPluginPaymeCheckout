<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'payme_log (
    id_log int(11) NOT NULL AUTO_INCREMENT,
    id_order int(11),
    authorizationResult VARCHAR(50),
    authorizationCode VARCHAR(50),
    errorCode VARCHAR(50),
    errorMessage VARCHAR(50),
    bin VARCHAR(50),
    brand VARCHAR(50),
    paymentReferenceCode VARCHAR(50),
    purchaseOperationNumber VARCHAR(50),
    purchaseAmount VARCHAR(50),
    purchaseCurrencyCode VARCHAR(50),
    purchaseVerification VARCHAR(50),
    plan VARCHAR(50),
    cuota VARCHAR(50),
    montoAproxCuota VARCHAR(50),
    resultadoOperacion VARCHAR(50),
    paymethod VARCHAR(20),
    fechaHora VARCHAR(50),
    reserved1 VARCHAR(50),
    reserved2 VARCHAR(50),
    reserved3 VARCHAR(50),
    reserved4 VARCHAR(50),
    reserved5 VARCHAR(50),
    reserved6 VARCHAR(50),
    reserved7 VARCHAR(50),
    reserved8 VARCHAR(50),
    reserved9 VARCHAR(50),
    reserved10 VARCHAR(50),
    numeroCip VARCHAR(50),
    PRIMARY KEY  (id_log)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';


$sql[] =  'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'payme_request (
            id_log int(11) NOT NULL AUTO_INCREMENT,
            reserved1 VARCHAR(50),
            reserved2 VARCHAR(50),
            reserved3 VARCHAR(50),
            reserved4 VARCHAR(50),
            reserved5 VARCHAR(50),
            purchaseOperationNumber VARCHAR(50),
            purchaseAmount VARCHAR(50),
            purchaseCurrencyCode VARCHAR(50),
            language VARCHAR(50),
            billingFirstName VARCHAR(50),
            billingLastName VARCHAR(50),
            billingEmail VARCHAR(50),
            billingAddress VARCHAR(50),
            billingZip VARCHAR(50),
            billingCity VARCHAR(50),
            billingState VARCHAR(50),
            billingCountry VARCHAR(50),
            billingPhone VARCHAR(50),
            shippingFirstName VARCHAR(50),
            shippingLastName VARCHAR(50),
            shippingEmail VARCHAR(50),
            shippingAddress VARCHAR(50),
            shippingZip VARCHAR(50),
            shippingCity VARCHAR(50),
            shippingState VARCHAR(50),
            shippingCountry VARCHAR(50),
            shippingPhone VARCHAR(50),
            programmingLanguage VARCHAR(50),
            userCommerce VARCHAR(50),
            userCodePayme VARCHAR(50),
            descriptionProducts VARCHAR(50),
            purchaseVerification VARCHAR(50),
            type VARCHAR(1),
            PRIMARY KEY  (id_log)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
