<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class PaymeReturnModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;
    
    public function initContent()
    {
        parent::initContent();
        
        $return = array();
    
        $cart = new Cart((int)substr(Tools::getValue('purchaseOperationNumber'),3,6));
        $customer = new Customer($cart->id_customer);
        
        
        if ($cart->id_customer == 0 ||
            $cart->id_address_delivery == 0 ||
            $cart->id_address_invoice == 0 ||
            !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == $this->module->name) {
                $authorized = true;
                break;
            }
        }
        
        if (!$authorized) {
            die($this->module->l('El Método de pago seleccionado no está disponible por el momento.', 'validation'));
        }
        
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        
        $return['authorizationResult'] =
            trim(Tools::getValue('authorizationResult')) == "" ? "-" : Tools::getValue('authorizationResult');
        $return['authorizationCode'] =
            trim(Tools::getValue('authorizationCode')) == "" ? "-" : Tools::getValue('authorizationCode');
        $return['errorCode'] = trim(Tools::getValue('errorCode')) == "" ? "-" : Tools::getValue('errorCode');
        $return['errorMessage'] = trim(Tools::getValue('errorMessage')) == "" ? "-" : Tools::getValue('errorMessage');
        $return['bin']                = trim(Tools::getValue('bin')) == "" ? "-" : Tools::getValue('bin');
        $return['brand']              = trim(Tools::getValue('brand')) == "" ? "-" : Tools::getValue('brand');
        $return['paymentReferenceCode'] =
            trim(Tools::getValue('paymentReferenceCode')) == "" ? "-" : Tools::getValue('paymentReferenceCode');
        $return['purchaseOperationNumber'] = Tools::getValue('purchaseOperationNumber');
        $return['purchaseAmount']     = Tools::getValue('purchaseAmount');
        $return['purchaseCurrencyCode'] = Tools::getValue('purchaseCurrencyCode');
        $return['purchaseVerification'] = Tools::getValue('purchaseVerification');
        $return['plan']               = Tools::getValue('plan');
        $return['cuota']              = Tools::getValue('cuota'); // solo envia cuanmdo se usa
        $return['montoAproxCuota']    = Tools::getValue('montoAproxCuota');
        $return['fechaHora']          = date("d/m/Y H:i:s");
        $return['reserved1']          = trim(Tools::getValue('reserved1')) == "" ? "-" : Tools::getValue('reserved1');
        $return['reserved2']          = trim(Tools::getValue('reserved2')) == "" ? "-" : Tools::getValue('reserved2');
        $return['reserved3']          = trim(Tools::getValue('reserved3')) == "" ? "-" : Tools::getValue('reserved3');
        $return['reserved4']          = trim(Tools::getValue('reserved4')) == "" ? "-" : Tools::getValue('reserved4');
        $return['reserved5']          = trim(Tools::getValue('reserved5')) == "" ? "-" : Tools::getValue('reserved5');
        $return['reserved6']          = trim(Tools::getValue('reserved6')) == "" ? "-" : Tools::getValue('reserved6');
        $return['reserved7']          = trim(Tools::getValue('reserved7')) == "" ? "-" : Tools::getValue('reserved7');
        $return['reserved8']          = trim(Tools::getValue('reserved8')) == "" ? "-" : Tools::getValue('reserved8');
        $return['reserved9']          = trim(Tools::getValue('reserved9')) == "" ? "-" : Tools::getValue('reserved9');
        $return['reserved10']         = trim(Tools::getValue('reserved10'))== "" ? "-" : Tools::getValue('reserved10');
        $return['numeroCip']          = trim(Tools::getValue('numeroCip')) == "" ? "-" : Tools::getValue('numeroCip');
        $return['paymethod']          = trim(Tools::getValue('brand')) == "" ? "-" : Tools::getValue('brand');
        
        if ($return['purchaseCurrencyCode'] == 840) {
            $this->module->idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE_DLLS');
            $this->module->keywallet = Configuration::get('ALIGNET_KEYWALLET_DLLS');
            $this->module->acquirerId = Configuration::get('ALIGNET_IDACQUIRER_DLLS');
            $this->module->idCommerce = Configuration::get('ALIGNET_IDCOMMERCE_DLLS');
            $this->module->key = Configuration::get('ALIGNET_KEY_DLLS');
        } else {
            $this->module->idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE');
            $this->module->keywallet = Configuration::get('ALIGNET_KEYWALLET');
            $this->module->acquirerId = Configuration::get('ALIGNET_IDACQUIRER');
            $this->module->idCommerce = Configuration::get('ALIGNET_IDCOMMERCE');
            $this->module->key = Configuration::get('ALIGNET_KEY');
        }
        
        switch ($return['authorizationResult']) {
            case '00':
                $return['resultadoOperacion'] = 'Transacción Autorizada';
                $paystate = 'PS_OS_PAYMENT';
                break;
            case '01':
                $return['resultadoOperacion'] = 'Transacción Denegada';
                $paystate = 'PS_OS_CANCELED';
                break;
            case '03':
                // echo "paystate = PAYME_OS_PENDENT";
                $return['resultadoOperacion'] = 'Transacción Pendiente';
                $paystate = 'PAYME_PENDENT';
                break;
            case '04':
                // echo "paystate = PAYME_PENDENT";
                $return['resultadoOperacion'] = 'Transacción Pendiente';
                $paystate = 'PAYME_PENDENT';
                break;
            case '05':
                if ($return['errorCode'] == '2300') {
                    $return['resultadoOperacion'] = 'Transacción Cancelada';
                } else {
                    $return['resultadoOperacion'] = 'Transacción Rechazada';
                }
                $paystate = 'PS_OS_CANCELED';
                break;
            default:
                $return['resultadoOperacion'] = 'Transacción incompleta';
                $paystate = 'PS_OS_ERROR';
        }

        $generatedPurchaseVerification = $this->module->purchaseVerification(
            $return['purchaseOperationNumber'],
            $return['purchaseAmount'],
            $return['purchaseCurrencyCode'],
            $return['authorizationResult']
        );

        
        $purchaseVerification = $return['purchaseVerification'];
      
        switch ($purchaseVerification) {
            case $generatedPurchaseVerification:
                
                if (!$cart->orderExists()) {
                    
                    $this->module->validateOrder($cart->id, Configuration::get($paystate), (float)$cart->getordertotal(true), $this->module->displayName, null, null, $cart->id_currency, false, $customer->secure_key);
                    $order = new Order($this->module->currentOrder);
                } else {
                  
                    $order = new Order((int)Order::getOrderByCartId($cart->id));
                    $current_state = (_PS_VERSION_ < '1.5') ? $order->getCurrentState() : $order->current_state;
                    if ($current_state != Configuration::get($paystate)) {
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState((int)Configuration::get($paystate), $order->id);
                        $history->addWithemail(true);
                    }
                }
         
                $return['id_order'] = Order::getOrderByCartId((int)$cart->id);
                break;
                
            default:
                $brand = $return['brand'];

                if ($brand == 6 || $brand == 25 || $brand == 7 || $brand== 34 ) {
                       
                      
                        
                        if ($brand == 6 || $brand == 25) {
                            $return['paymethod'] = "PAGO EFECTIVO";
                        } elseif ($brand == 7 || $brand== 34) {
                            $return['paymethod'] = "SAFETYPAY";
                        } else {
                            $return['paymethod'] = "-";
                        }

                        if (!$cart->orderExists()) {

                            $this->module->validateOrder($cart->id, Configuration::get($paystate), (float)$cart->getordertotal(true), $this->module->displayName, null, null, $cart->id_currency, false, $customer->secure_key);
                            $order = new Order($this->module->currentOrder);
                        } else {
              
                            $order = new Order((int)Order::getOrderByCartId($cart->id));
                            $current_state = (_PS_VERSION_ < '1.5') ? $order->getCurrentState() : $order->current_state;
                            if ($current_state != Configuration::get($paystate)) {
                                $history = new OrderHistory();
                                $history->id_order = (int)$order->id;
                                $history->changeIdOrderState((int)Configuration::get($paystate), $order->id);
                                $history->addWithemail(true);
                            }
                        }
              
                        $return['id_order'] = Order::getOrderByCartId((int)$cart->id);
                        break;
                }
                else
                {
                    $this->module->validateOrder($cart->id, Configuration::get('PS_OS_CANCELED'), (float)$cart->getordertotal(true), $this->module->displayName, null, null, $cart->id_currency, false, $customer->secure_key);
                    $return['id_order'] = Order::getOrderByCartId((int)$cart->id);
                }
               
                
                
        }

        Db::getInstance()->insert('payme_log', $return);
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.
            '&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
    }
}
