## [2.0.7] - 27-Dec-2018
### Added
- Correcciones de textos y titulos.


## [2.0.6] - 12-Dec-2018
### Added
- Se corrigio validación que obligaba a escoger moneda local. (para casos donde la moneda local es USD.


## [2.0.5] - 12-Dec-2018
### Added
- Correcciones de texto y funcionalidad pago Efectivo.


## [2.0.4] - 12-Dec-2018
### Added
- Se Agregó compatibilidad 1.6.1.23
- cambios menores
- Compatibilidad 
	- Compatibilidad : Prestashop 1.6.1.23 - 1.7.4.3
	- PHP : 5.6.+ y 7.0.+
	- Version : 2.0.4
- Se agregaron campos reservados hasta 10


## [2.0.3] - 06-Dec-2018
### Added
- Se agregó tabla para guardar el log y los carritos y poderlo mostrar en el detalle del pedido
- cambios menores
- Compatibilidad 
	- Compatibilidad : Prestashop 7.1.4.2
	- PHP : 5.6.+ y 7.0.+
	- Version : 2.0.3
- ID WALLET Y CLAVE WALLET son opcionales.
- Se liberaron los campos reservados.


## [2.0.2] - 28-Nov-2018
### Added
- Se cambio de texto "Pagar con Tarjeta u Otros" por "Pagar con Tarjeta de Crédito, Débito u otros medios".


## [2.0.1] - 26-Nov-2018
### Added
- Se agrego esquema redirect 
- Ahora puede seleccionar diseño de modal
- Se envian campos reservados.
		CMS
		Version de CMS
		Nombre de PluginPrestashop
		Version PluginPrestashop

		
## [2.0.0] - 26-Nov-2018
### Added
- Compatibilidad con Version 1.7










