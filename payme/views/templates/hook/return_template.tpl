 {*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="col-sm-12">
			<h2 class="title {$status|escape:'htmlall':'UTF-8'} text-center">
				<i class="fa fa-check-circle" aria-hidden="true"></i> {$r_query.resultadoOperacion|escape:'htmlall':'UTF-8'}
			</h2>
		</div>
		
		{if $status == 'success' || $status == 'warning'}

			<br><br>
			
			{if $r_query.brand == '6' || $r_query.brand == '25'}
			<section class="col-sm-12 text-center clearfix">
				<div class="col-sm-3 col-sm-offset-1">
					<img src="{$views|escape:'htmlall':'UTF-8'}img/pagoefectivo.png" height="100px">
				</div>
				<div class="col-sm-8 text-left">
					<p>Su transacción 
						<a href="{$link->getPageLink('order-detail', false)|escape:'html'}&id_order={$order->id}"
							class="idorder">
							{$order->id|str_pad:9:'0':$smarty.const.STR_PAD_LEFT|escape:'htmlall':'UTF-8'}</a> 
							se encuentra pendiente de pago. Por favor acérquese a la agencia bancaria más cercana para 
							realizar el pago con el siguiente código:</p>
					<p class="pagoefectivo-cip">CIP: <b>{$r_query.numeroCip|escape:'htmlall':'UTF-8'}</b></p> 
				</div>
			</section>
			<hr>
			{elseif $r_query.brand == '7' || $r_query.brand == '34'}
			<section class="col-sm-12 text-center clearfix">
				<div class="col-sm-3 col-sm-offset-1">
					<img src="{$views|escape:'htmlall':'UTF-8'}img/safetypay.png"  height="100px">
				</div>
				<div class="col-sm-8 text-left">
					<p>Su transacción 
						<a href="{$link->getPageLink('order-detail', false)|escape:'html'}&id_order={$order->id}"
							class="idorder">
							{$order->id|str_pad:9:'0':$smarty.const.STR_PAD_LEFT|escape:'htmlall':'UTF-8'}</a> 
							se encuentra pendiente de pago. Por favor acérquese a la agencia bancaria más cercana para 
							realizar el pago.</p>
				</div>
			</section>
			<hr>
			{else}
			<p class="text-center">
				Su transacción con número de pedido
				<a href="{$link->getPageLink('order-detail', false)|escape:'html'}&id_order={$order->id}" class="idorder">
				{$order->id|str_pad:9:'0':$smarty.const.STR_PAD_LEFT|escape:'htmlall':'UTF-8'}</a> fue autorizada con éxito.
			</p>
			<hr>
			{/if}
			
			<div class="row">
				<section class="address-delivery col-sm-4 col-sm-offset-1">
					<h4 class="subtitle"><i class="icon icon-cc-visa" aria-hidden="true"></i><b> Respuesta de Pay-me</b></h4>
					<ul>
						{if $r_query.brand != '-'}<li><b>Marca: </b>{$r_query.paymethod|escape:'htmlall':'UTF-8'}</li>{/if}
						{if $r_query.paymentReferenceCode != '-'}
							<li><b>Tarjeta: </b>{$r_query.paymentReferenceCode|escape:'htmlall':'UTF-8'} </li> 
						{/if}
						{if $r_query.resultadoOperacion != '-'}
							<li><b> Respuesta: </b> {$r_query.resultadoOperacion|escape:'htmlall':'UTF-8'} </li>
						{/if}
					</ul>
				</section>
				<section class="payment-datails col-sm-4 col-sm-offset-1">
					<h4 class="subtitle"><i class="icon icon-credit-card"></i><b> Detalles de pago</b></h4>
					<ul>
						<li class="total">
							Número de operación:
							<span class="pull-right"> {$r_query.purchaseOperationNumber|escape:'htmlall':'UTF-8'}</span>
						</li>
						<li class="total">
							Importe Total: <span class="pull-right"> {$total|escape:'htmlall':'UTF-8'}</span>
						</li>
					</ul>
				</section>
			</div>
			<br>
			<div class="row">
				<section class="details col-sm-12">
					<br>
					<p>Este pedido fue generado el {$order->date_add|date_format:"%D"|escape:'htmlall':'UTF-8'} a las 
						{$order->date_add|date_format:"%I:%M %p"|escape:'htmlall':'UTF-8'}, en breve recibirá un correo a 
						{$email|escape:'htmlall':'UTF-8'} con la confirmación del pago el cual debe imprimir y/o guardar.
					</p>
					<br>
				</section>
			</div>

		{else}
		
			<br><br>
			<hr>
			<div class="row">
				<section class="details col-sm-12">
					<p style="text-transform:uppercase"><b>{$fullname|escape:'htmlall':'UTF-8'}</b></p>
					<p> Su transacción con número de pedido
							<a href="{$link->getPageLink('order-detail', false)|escape:'html'}&id_order={$order->id}">
								<b>{$r_query.purchaseOperationNumber|escape:'htmlall':'UTF-8'}</b>
							</a>
							generada el {$order->date_add|date_format:"%D"|escape:'htmlall':'UTF-8'} a las
							{$order->date_add|date_format:"%I:%M %p"|escape:'htmlall':'UTF-8'} fue
						
						{if $r_query.authorizationResult == '01'}
						 	DENEGADA. <br>
							Por favor contáctese con su banco para validar la denegación de la transacción. <br>
							Tener presente que esta operación <b>NO HA GENERADO NINGUN COBRO</b> en su tarjeta.
						
						{elseif $r_query.authorizationResult == '05'}
							{if $r_query.errorCode == '2300'}
								CANCELADA. <br>
								Tener presente que esta operación <b>NO HA GENERADO NINGUN COBRO</b> en su tarjeta.
							{else}
								RECHAZADA. <br>
								Por favor contáctese con la tienda para validar el rechazo de la transacción. <br>
								Tener presente que esta operación <b>NO HA GENERADO NINGUN COBRO</b> en su tarjeta”.
							{/if}
						{/if}
					</p>
				</section>
			</div>

		{/if}
		<table class="table">
			<thead>
				<tr>

					<th>Productos del pedido:</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$productos item=product}
				<tr>
					<td>- {$product.name|escape:'htmlall':'UTF-8'}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<section class="col-sm-12">
		<h4 class="subtitle"><b><i class="icon icon-question-circle" aria-hidden="true"></i>¿Dudas o Consultas?</b></h4>
		<ul>
			<li><i class="fa fa-angle-double-right" aria-hidden="true"></i> {l s='Si deseas conocer acerca tus derechos
			como consumidor puedes ingresar a nuestra página de' mod='payme'}
				<a href="{$link_conditions|escape:'html':'UTF-8'}&content_only=1" class="iframe"target="_blank">
					{l s='Términos y Condiciones.' mod='payme'}
				</a>
			</li>
			<li>
				<i class="fa fa-angle-double-right" aria-hidden="true"></i>
				Contamos con un equipo de expertos que te ayudarán a resolver todas tus dudas o consultas presentadas
				respecto a este pedido. Ingresa a nuestro
				<a href="{$link->getPageLink('contact', true)|escape:'html'}">
					{l s='Centro de Atención al Cliente' mod='payme'}
				</a>.
			</li>
		</ul>
	</section>
</div>
<br><br><br><br>
{if $debug > 0}
<pre>{$r_query|print_r:true|escape:'htmlall':'UTF-8'}</pre>
{/if}

{literal}
<script type="text/javascript">
	var obj = document.getElementById("content-hook_order_confirmation");
        obj.style.display = "none";

    var content = document.getElementById("content");
    	content.style.display = "none";



</script>
{/literal}
