{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript" src="{$wsdomain|escape:'htmlall':'UTF-8'}/VPOS2/js/modalcomercio.js"></script>

{if $acquirerId == ''}
<p class="alert alert-danger">PAY-ME: El campo "ID Adquirente" se encuentra en blanco para este tipo de moneda </p>
{/if}

{if $idCommerce == ''}
<p class="alert alert-danger">PAY-ME: El campo "ID Commerce" se encuentra en blanco para este tipo de moneda </p>
{/if}

<div class="row">
    <div class="payme col-xs-12 col-md-12">
        <p class="payment_module">
            
                <img src="{$payme_img|escape:'htmlall':'UTF-8'}" />
            
        </p>
    </div>
</div>

<form name="f1" id="f1" action="#" method="post" class="alignet-form-vpos2">
    <input type="hidden" name="acquirerId" value="{$acquirerId|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="idCommerce" value="{$idCommerce|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="purchaseOperationNumber"
        value="{$commerce.purchaseOperationNumber|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="purchaseAmount" value="{$commerce.purchaseAmount|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="purchaseCurrencyCode" value="{$commerce.purchaseCurrencyCode|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="language" value="{$commerce.language|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingFirstName" value="{$commerce.shippingFirstName|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingLastName" value="{$commerce.shippingLastName|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingEmail" value="{$commerce.shippingEmail|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingAddress" value="{$commerce.shippingAddress|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingZIP" value="{$commerce.shippingZip|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingCity" value="{$commerce.shippingCity|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingState" value="{$commerce.shippingState|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="shippingCountry" value="{$commerce.shippingCountry|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingFirstName" value="{$commerce.billingFirstName|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingLastName" value="{$commerce.billingLastName|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingEmail" value="{$commerce.billingEmail|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingAddress" value="{$commerce.billingAddress|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingZIP" value="{$commerce.billingZip|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingCity" value="{$commerce.billingCity|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingState" value="{$commerce.billingState|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingCountry" value="{$commerce.billingCountry|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="billingPhone" value="{$commerce.billingPhone|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="userCommerce" value="{$commerce.userCommerce|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="userCodePayme" value="{$commerce.userCodePayme|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="descriptionProducts" value="{$commerce.descriptionProducts|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="programmingLanguage" value="{$commerce.programmingLanguage|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved1" value="{$commerce.reserved1|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved2" value="{$commerce.reserved2|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved3" value="{$commerce.reserved3|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved4" value="{$commerce.reserved4|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved5" value="{$commerce.reserved5|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved6" value="{$commerce.reserved1|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved7" value="{$commerce.reserved2|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved8" value="{$commerce.reserved3|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved9" value="{$commerce.reserved4|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="reserved10" value="{$commerce.reserved5|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="purchaseVerification" value="{$commerce.purchaseVerification|escape:'htmlall':'UTF-8'}">
</form>


{if $debug > 0}
<pre> {$commerce|print_r:true} </pre> {/if}
