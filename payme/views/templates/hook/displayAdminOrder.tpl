{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div class="panel">
	<div class="panel-heading">
		<i class="icon-credit-card"></i>
		{l s='Respuesta' mod='payme'}
		<span class="badge">PAY-ME</span>
	</div>
	
	<ul class="nav nav-tabs" id="tabModulePayment">
		<li class="active">
			<a href="#resume">
				{l s='Resumen' mod='payme'}
			</a>
		</li>
		<li>
			<a href="#request">
				<i class="icon-file-text"></i>
				{l s='Request Pay-me' mod='payme'}
			</a>
		</li>
		<li>
			<a href="#technical">
				<i class="icon-file-text"></i>
				{l s='Response Pay-me' mod='payme'}
			</a>
		</li>
		
	</ul>
	<div class="tab-content panel">
		<div class="tab-pane active clearfix" id="resume">
			<div class="col-sm-7 ">
			{foreach from=$results key=k item=i}
				{foreach from=$i item=s}
					<dl class="well list-detail">
						{if $s.purchaseOperationNumber != '-'}
							<dt>{l s='Número de Operación de Compra' mod='payme'}</dt>
							<dd>{$s.purchaseOperationNumber|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						{if $s.resultadoOperacion != '-'}
							<dt>{l s='Respuesta' mod='payme'}</dt>
							<dd>{$s.resultadoOperacion|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						{if $s.fechaHora != '-'}
							<dt>{l s='Fecha de Operación' mod='payme'}</dt>
							<dd>{$s.fechaHora|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						{if $s.paymentReferenceCode != '-'}
							<dt>{l s='Número de Tarjeta' mod='payme'}</dt>
							<dd>{$s.paymentReferenceCode|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						{if $s.paymethod != '-'}
							<dt>{l s='Método de Pago' mod='payme'}</dt>
							<dd> {$s.paymethod|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						{if $s.numeroCip != '-'}
							<dt>{l s='CIP' mod='payme'}</dt>
							<dd>{$s.numeroCip|escape:'htmlall':'UTF-8'}</dd>
						{/if}
						<dt>{l s='Status' mod='payme'}</dt>
						<dd>
							<span class="badge {if $s.authorizationResult == '00'}badge-success{else}badge-danger{/if}">
								{$s.resultadoOperacion|escape:'htmlall':'UTF-8'}
							</span>
						</dd>
					</dl>
				{/foreach}
			{/foreach}
			</div>
		</div>

		<div class="tab-pane" id="request">
			{foreach from=$resultRequest key=k item=i}
	    		<pre>{$i|print_r:true|escape:'htmlall':'UTF-8'}</pre>
			{/foreach}
		</div>
		<div class="tab-pane" id="technical">
			{foreach from=$results key=k item=i}
	    		<pre>{$i|print_r:true|escape:'htmlall':'UTF-8'}</pre>
			{/foreach}
		</div>
	</div>
	
</div>

<script>
	$('#tabModulePayment a').click(function (e) {
		e.preventDefault()
		$(this).tab('show')
	})
</script>
