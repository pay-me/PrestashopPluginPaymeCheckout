{if $acquirerId == ''}
<p class="alert alert-danger">PAY-ME: El campo "ID Adquirente" se encuentra en blanco para este tipo de moneda </p>
{/if}

{if $idCommerce == ''}
<p class="alert alert-danger">PAY-ME: El campo "ID Commerce" se encuentra en blanco para este tipo de moneda </p>
{/if}

<div class="row">
    <div class="payme col-xs-12 col-md-12">
        <p class="payment_module">
            
                <img src="{$payme_img|escape:'htmlall':'UTF-8'}" />
            
        </p>
    </div>
</div>


{if $debug > 0}
<pre> {$commerce|print_r:true} </pre> {/if}
