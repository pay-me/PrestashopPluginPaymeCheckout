<?php

/**
*  @author    ALIGNET
*/


/*This checks for the Prestashop existence */
if (!defined('_PS_VERSION_')) {
    exit;
}

class Payme extends PaymentModule
{
	private	$_html = '';
	private $_postErrores = array();

	public function __construct(){
		$this->name = 'payme';
		$this->tab = 'payments_gateways';
		$this->version ='2.0.7';
		$this->subversion ='Stable';
		$this->bootstrap = true;
		$this->module_key = '2242729b0888a1184def9c91as426d357';
		$this->domain = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;
		$this->views = _MODULE_DIR_.$this->name.'/views/';
		$this->url_return = $this->domain.'index.php?fc=module&module='.$this->name.'&controller=return';

		parent::__construct();

		$this->page = basename(__FILE__, '.php');
		$this->author = "ALIGNET";
		$this->displayName = $this->l('Pay-me Checkout');
		$this->description = $this->l('Vende en línea con la pasarela de pagos Pay-me. Ofrece una buena experiencia de compra a tus clientes, acepta diferentes medios de pagos y haz que tu negocio crezca.');
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		$this->currencies_mode = 'radio';
		$this->confirmUninstall = $this->l('¿Seguro de desisntalar el módulo?');
		$this->env = Configuration::get('ALIGNET_URLTPV');
		$this->esquema = Configuration::get('ALIGNET_ESQUEMA');
		$this->tipomodal = Configuration::get('ALIGNET_TIPOMODAL');

		if ($this->esquema == 0) {
			switch ($this->env) {
	            case 'INTEGRACION':
	                $this->wsdomain = 'https://integracion.alignetsac.com';
	                $this->wsdl = $this->wsdomain.'/WALLETWS/services/WalletCommerce?wsdl';
	                $this->modalVPOS2 = "javascript:AlignetVPOS2.openModal('https://integracion.alignetsac.com/','".$this->tipomodal."')";
	                break;
	            case 'PRODUCCION':
	                $this->wsdomain = 'https://vpayment.verifika.com';
	                $this->wsdl = "https://www.pay-me.pe/WALLETWS/services/WalletCommerce?wsdl";
	                $this->modalVPOS2 = "javascript:AlignetVPOS2.openModal('https://vpayment.verifika.com/','".$this->tipomodal."')";
	                break;
	        }
		}
		else
		{
			switch ($this->env) {
	            case 'INTEGRACION':
	                $this->wsdomain = 'https://integracion.alignetsac.com';
	                $this->wsdl = $this->wsdomain.'/WALLETWS/services/WalletCommerce?wsdl';
	                $this->modalVPOS2 = "https://integracion.alignetsac.com/VPOS2/faces/pages/startPayme.xhtml";
	                break;
	            case 'PRODUCCION':
	                $this->wsdomain = 'https://vpayment.verifika.com';
	                $this->wsdl = "https://www.pay-me.pe/WALLETWS/services/WalletCommerce?wsdl";
	                $this->modalVPOS2 = "https://vpayment.verifika.com/VPOS2/faces/pages/startPayme.xhtml";
	                break;
       		}
		}
	}

	public function install() {

		include(dirname(__FILE__).'/sql/install.php');

		$this->addOrderState('PAYME_PENDENT');
        if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('displayAdminOrderLeft') || !$this->registerHook('Payment') || !$this->registerHook('paymentReturn') || (version_compare(_PS_VERSION_, '1.7', '>=') && !$this->registerHook('paymentOptions')) || !Configuration::updateValue('ALIGNET_TIPOMODAL', '1')) {
            return false;
        }
        return true;
	}

	public function uninstall()	{

		if (!Configuration::deleteByName('ALIGNET_URLTPV')
			|| !Configuration::deleteByName('ALIGNET_ESQUEMA')
			|| !Configuration::deleteByName('ALIGNET_TIPOMODAL')
			|| !Configuration::deleteByName('ALIGNET_IDACQUIRER')
			|| !Configuration::deleteByName('ALIGNET_IDCOMMERCE')
			|| !Configuration::deleteByName('ALIGNET_KEY')
			|| !Configuration::deleteByName('ALIGNET_IDENTCOMMERCE')
			|| !Configuration::deleteByName('ALIGNET_KEYWALLET')
			|| !Configuration::deleteByName('ALIGNET_IDACQUIRER_DLLS')
			|| !Configuration::deleteByName('ALIGNET_IDCOMMERCE_DLLS')
			|| !Configuration::deleteByName('ALIGNET_KEY_DLLS')
			|| !Configuration::deleteByName('ALIGNET_IDENTCOMMERCE_DLLS')
			|| !Configuration::deleteByName('ALIGNET_KEYWALLET_DLLS')
			|| !Configuration::deleteByName('ALIGNET_DEBUG')
			|| !Configuration::deleteByName('ALIGNET_REDIR')
			|| !Configuration::deleteByName('PAYME_OS_PAID')
			|| !Configuration::deleteByName('PAYME_OS_FAILED')
			|| !parent::uninstall())
			return false;
		return true;
	}

	private function _postValidacion() {
		if (isset($_POST['btnSubmit'])) {
			
			if (!empty($_POST['ALIGNET_IDENTCOMMERCE']) && !is_numeric($_POST['ALIGNET_IDENTCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Wallet" sólo permite números!');
			}
		
			if (empty($_POST['ALIGNET_IDACQUIRER'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Adquirente" no puede estar vacío!');
			}
			if (!empty($_POST['ALIGNET_IDACQUIRER']) && !is_numeric($_POST['ALIGNET_IDACQUIRER'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Adquirente" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_IDCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Comercio" no puede estar vacío!');
			}
			if (!empty($_POST['ALIGNET_IDCOMMERCE']) && !is_numeric($_POST['ALIGNET_IDCOMMERCE'])) {
				$this->_postErrores[] = $this->l('El campo requerido "ID Comercio" sólo permite números!');
			}
			if (empty($_POST['ALIGNET_KEY'])) {
				$this->_postErrores[] = $this->l('El campo requerido "Clave V-POS2" no puede estar vacío!');
			}
		}
	}

	public function getContent() {

		if ($this->postValidation()) {
            $this->postProcess();
        } else {
            // foreach ($this->postErrors as $err) {
            //     $this->html .= $this->displayError($err);
            // }
        }
        $this->html .= $this->displayConfiguration();
        $this->html .= $this->renderForm();   
        return $this->html;
	}
  public function hookPaymentOptions($params)
  {
      return $this->hookPayment($params);
  }
	public function hookPayment($params) {

		if (!$this->active) {
            return;
        }

    $commerce = $this->getParameteresCommerce(); 

    if ($this->idCommerce == '' || $this->acquirerId == '' ) {
       return;
    }
        if (version_compare(_PS_VERSION_, '1.7', '>='))
        {
             $payment_options = [
            $this->getEmbeddedPaymentOption(),
                ];
           return $payment_options;
        }
        else
        {
          
          Db::getInstance()->delete('payme_request', "purchaseOperationNumber = '". $commerce['purchaseOperationNumber']."'");
          Db::getInstance()->insert('payme_request', $commerce);
          $module_dir = _PS_MODULE_DIR_ . $this->name . '/';
          $ALIGNET_IDACQUIRER = Configuration::get('ALIGNET_IDACQUIRER');

          if ($ALIGNET_IDACQUIRER == 29 || $ALIGNET_IDACQUIRER == 144 || $ALIGNET_IDACQUIRER == 84 || $ALIGNET_IDACQUIRER == 10) {
                  $payme_img = 'views/img/banner_payme_peru.png';
          } else {
               $payme_img = 'views/img/banner_payme_latam.png';
          }

          $this->context->smarty->assign(array(
              'module_dir' => $module_dir,
              'payme_img' => $payme_img,
              'commerce' => $this->getParameteresCommerce(),
              'acquirerId' => $this->acquirerId,
              'idCommerce' => $this->idCommerce,
              'wsdomain' => $this->wsdomain,
              'debug' => Configuration::get('ALIGNET_DEBUG'),
              'modalVPOS2' => $this->modalVPOS2,
              'esquema' => $this->esquema
          ));

            return $this->display(__FILE__, 'views/templates/hook/payment16.tpl');
        }
 }

	public function getEmbeddedPaymentOption()
    {

    	$module_dir = _MODULE_DIR_ . $this->name ;
		  $ALIGNET_IDACQUIRER = Configuration::get('ALIGNET_IDACQUIRER');

        if ($ALIGNET_IDACQUIRER == 29 || $ALIGNET_IDACQUIRER == 144 || $ALIGNET_IDACQUIRER == 84 || $ALIGNET_IDACQUIRER == 10) {
             $payme_img = $module_dir .'/views/img/banner_payme_peru.png';
        } 
        else 
        {
             $payme_img = $module_dir .'/views/img/banner_payme_latam.png';
        }

         $commerce = $this->getParameteresCommerce();
        $this->context->smarty->assign([
           
          	'module_dir' => $module_dir,
            'payme_img' => $payme_img,
            'commerce' => $commerce,
            'acquirerId' => $this->acquirerId,
            'idCommerce' => $this->idCommerce,
            'wsdomain' => $this->wsdomain,
            'debug' => Configuration::get('ALIGNET_DEBUG'),
            'modalVPOS2' => $this->modalVPOS2,
           
        ]);

        
       
      	 Db::getInstance()->delete('payme_request', "purchaseOperationNumber = '". $commerce['purchaseOperationNumber']."'");

         Db::getInstance()->insert('payme_request', $commerce);

        $embeddedOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();

        
        if ($this->esquema == 0) {

        	$embeddedOption->setCallToActionText($this->l('Pagar con Tarjeta de Crédito, Débito u otros medios.'))
                           ->setAdditionalInformation($this->context->smarty->fetch('module:payme/views/templates/hook/payment.tpl'))
	                   	   ->setAction($this->modalVPOS2);
	        return $embeddedOption;
        }
        else
        {
        	 $paymentForm = $this->fetch($this->getTemplate('front', 'payment.tpl'));
             $embeddedOption->setCallToActionText($this->l('Pagar con Tarjeta de Crédito, Débito u otros medios.'))
                       ->setAdditionalInformation($this->fetch($this->getTemplate('front', 'payment_info.tpl')))
                       ->setForm($paymentForm)
                   	   ->setAction($this->modalVPOS2);
             return $embeddedOption;
        }        
    }

	public function hookPaymentReturn($params) {
     
     if (!$this->active) {
            return;
        }

        if (version_compare(_PS_VERSION_, '1.7', '>='))
        {
           $order = $params['order'];
        }
        else
        {
           $order = $params['objOrder'];
        }
      
        $orderstate = new OrderState($order->current_state);
        $cart = new Cart($order->id_cart);
        
        $sql = 'SELECT * FROM '._DB_PREFIX_.$this->name.'_log WHERE id_order='.$order->id;
        $r_query = Db::getInstance()->getRow($sql);
        

       if (version_compare(_PS_VERSION_, '1.7', '>='))
       {
           if (in_array($params['order']->getCurrentState(), array(Configuration::get('PS_OS_PAYMENT'), Configuration::get('PS_OS_OUTOFSTOCK'), Configuration::get('PS_OS_OUTOFSTOCK_UNPAID'),Configuration::get('PAYME_PENDENT')))) {
            $this->smarty->assign('status', 'success');
            } elseif ($params['order']->getCurrentState() == Configuration::get('PS_OS_COD_VALIDATION')) {
                $this->smarty->assign('status', 'warning');
            } else {
                 $this->smarty->assign('status', 'danger');
            }
        }
        else
        {
           if (in_array($params['objOrder']->getCurrentState(), array(Configuration::get('PS_OS_PAYMENT'), Configuration::get('PS_OS_OUTOFSTOCK'), Configuration::get('PS_OS_OUTOFSTOCK_UNPAID'),Configuration::get('PAYME_PENDENT')))) {
            $this->smarty->assign('status', 'success');
            } elseif ($params['objOrder']->getCurrentState() == Configuration::get('PS_OS_COD_VALIDATION')) {
                $this->smarty->assign('status', 'warning');
            } else {
                 $this->smarty->assign('status', 'danger');
            }
        }

       
        
        
        $currency = new Currency($cart->id_currency);
        
        $cms_condiions = new CMS(Configuration::get('PS_CONDITIONS_CMS_ID'), $this->context->language->id);
         
       

       if (version_compare(_PS_VERSION_, '1.7', '>='))
       {
         $this->context->smarty->assign(array(
            'order' => $order,
            'ordstate' => $orderstate->name[$this->context->language->id],
            'productos' => $cart->getProducts(),
            'r_query' => $r_query,
            'fullname' => $this->context->cookie->customer_firstname.' '.$this->context->cookie->customer_lastname,
            'email' => $this->context->cookie->email,
            'link_conditions' => $this->context->link->getCMSLink($cms_condiions, $cms_condiions->link_rewrite, Configuration::get('PS_SSL_ENABLED')),
            'total' => Tools::displayPrice($params['order']->total_paid, $currency, false), 
            'debug' => Configuration::get('ALIGNET_DEBUG'),
            'views' => $this->views,
        ));
        return $this->fetch('module:payme/views/templates/hook/return_template.tpl');
       }
       else
       {
          $this->context->smarty->assign(array(
            'order' => $order,
            'ordstate' => $orderstate->name[$this->context->language->id],
            'productos' => $cart->getProducts(),
            'r_query' => $r_query,
            'fullname' => $this->context->cookie->customer_firstname.' '.$this->context->cookie->customer_lastname,
            'email' => $this->context->cookie->email,
            'link_conditions' => $this->context->link->getCMSLink($cms_condiions, $cms_condiions->link_rewrite, Configuration::get('PS_SSL_ENABLED')),
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
            'debug' => Configuration::get('ALIGNET_DEBUG'),
            'views' => $this->views,
        ));

           return $this->display(__FILE__, 'return_template.tpl');
       }

  }

    protected function getConfigFormValues()
    {
        return array(
            'ALIGNET_URLTPV' => Tools::getValue('ALIGNET_URLTPV', Configuration::get('ALIGNET_URLTPV')),
            'ALIGNET_TIPOMODAL' => Tools::getValue('ALIGNET_TIPOMODAL', Configuration::get('ALIGNET_TIPOMODAL')),
            'ALIGNET_ESQUEMA' => Tools::getValue('ALIGNET_ESQUEMA', Configuration::get('ALIGNET_ESQUEMA')),
            'ALIGNET_IDACQUIRER' => Tools::getValue('ALIGNET_IDACQUIRER', Configuration::get('ALIGNET_IDACQUIRER')),
            'ALIGNET_IDCOMMERCE' => Tools::getValue('ALIGNET_IDCOMMERCE', Configuration::get('ALIGNET_IDCOMMERCE')),
            'ALIGNET_IDENTCOMMERCE' => Tools::getValue('ALIGNET_IDENTCOMMERCE', Configuration::get('ALIGNET_IDENTCOMMERCE')),
            'ALIGNET_KEY' => Tools::getValue('ALIGNET_KEY', Configuration::get('ALIGNET_KEY')),
            'ALIGNET_KEYWALLET' => Tools::getValue('ALIGNET_KEYWALLET', Configuration::get('ALIGNET_KEYWALLET')),
            'ALIGNET_IDCOMMERCE_DLLS' => Tools::getValue('ALIGNET_IDCOMMERCE_DLLS', Configuration::get('ALIGNET_IDCOMMERCE_DLLS')),
            'ALIGNET_IDACQUIRER_DLLS' => Tools::getValue('ALIGNET_IDACQUIRER_DLLS', Configuration::get('ALIGNET_IDACQUIRER_DLLS')),
            'ALIGNET_IDENTCOMMERCE_DLLS' => Tools::getValue('ALIGNET_IDENTCOMMERCE_DLLS', Configuration::get('ALIGNET_IDENTCOMMERCE_DLLS')),
            'ALIGNET_KEY_DLLS' => Tools::getValue('ALIGNET_KEY_DLLS', Configuration::get('ALIGNET_KEY_DLLS')),
            'ALIGNET_KEYWALLET_DLLS' => Tools::getValue('ALIGNET_KEYWALLET_DLLS', Configuration::get('ALIGNET_KEYWALLET_DLLS')),
            'ALIGNET_DEBUG' => Tools::getValue('ALIGNET_DEBUG', Configuration::get('ALIGNET_DEBUG')),
            'FREE' => Tools::getValue('FREE', Configuration::get('FREE')),
        );
    }


     public function hookdisplayAdminOrderLeft()
    {


        $order_current = Tools::getValue('id_order');
        $sql_2 = 'SELECT 
                  id_order,
                  authorizationResult,
                  authorizationCode,
                  errorCode,
                  errorMessage,
                  bin,
                  brand,
                  paymentReferenceCode,
                  purchaseOperationNumber,
                  purchaseAmount,
                  purchaseCurrencyCode,
                  purchaseVerification,
                  plan,
                  cuota,
                  montoAproxCuota,
                  resultadoOperacion,
                  paymethod,
                  fechaHora,
                  reserved1,
                  reserved2,
                  reserved3,
                  reserved4,
                  reserved5,
                  reserved6,
                  reserved7,
                  reserved8,
                  reserved9,
                  reserved10,
                  numeroCip
             FROM '._DB_PREFIX_.$this->name.'_log WHERE id_order='.$order_current;
        $results = Db::getInstance()->ExecuteS($sql_2);



        $results[0]['purchaseOperationNumber'] = (int)substr($results[0]['purchaseOperationNumber'], 3,6);

      

        $QueryRequest = 'SELECT programmingLanguage,
                          userCommerce,
                          userCodePayme,
                          descriptionProducts,
                          purchaseVerification,
                          purchaseOperationNumber,
                          purchaseAmount,
                          purchaseCurrencyCode,
                          language,
                          billingFirstName,
                          billingLastName,
                          billingEmail,
                          billingAddress,
                          billingZip,
                          billingCity,
                          billingState,
                          billingCountry,
                          billingPhone,
                          shippingFirstName,
                          shippingLastName,
                          shippingEmail,
                          shippingAddress,
                          shippingZip,
                          shippingCity,
                          shippingState,
                          shippingCountry,
                          shippingPhone,
                          reserved1,
                          reserved2,
                          reserved3,
                          reserved4,
                          reserved5,
                          reserved6,
                          reserved7,
                          reserved8,
                          reserved9,
                          reserved10 FROM 
                          '._DB_PREFIX_.$this->name."_request WHERE purchaseOperationNumber = ". $results[0]['purchaseOperationNumber']."";
        $resultRequest = Db::getInstance()->ExecuteS($QueryRequest);
        
        $this->context->smarty->assign(array(
            'order_current' => $order_current,
            'results' => array($results),
            'views' => $this->views,
            'resultRequest'=>$resultRequest
        ));
        return $this->display(__FILE__, 'displayAdminOrder.tpl');
    }



    public function hookHeader($params)
    {
        $this->context->controller->addCSS(($this->_path).'views/css/payme.css');
    }

	public function renderForm()
    {

    	if ((Tools::getValue('ALIGNET_URLTPV') == "") and (isset($this->env)))
			$ambiente = $this->env;
		else
			$ambiente = Tools::getValue('ALIGNET_URLTPV');


		$ambiente_integracion = ($ambiente == 'INTEGRACION') ? ' selected="selected" ' : '';
		$ambiente_produccion = ($ambiente == 'PRODUCCION') ? ' selected="selected" ' : '';

       	$html = '<form id="module_form" class="defaultForm form-horizontal" action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data" novalidate="">
   <input type="hidden" name="btnSubmitPayme" value="1">
   <div class="panel" id="fieldset_0">
      <div class="panel-heading">
         <i class="icon-cog"></i>							CONFIGURACIÓN GENERAL
      </div>
      <div class="form-wrapper">
         <div class="form-group">
            <label class="control-label col-lg-3">
            Ambiente
            </label>
            <div class="col-lg-2">
              <select  name="ALIGNET_URLTPV">
												<option value="INTEGRACION"' . $ambiente_integracion . '>' . $this->l('Integración') . '</option>
												<option value="PRODUCCION"' . $ambiente_produccion . '>' . $this->l('Producción') . '
											</option></select>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            Depuración
            </label>
            <div class="col-lg-9">
               <span class="switch prestashop-switch fixed-width-lg">
               <input type="radio" name="ALIGNET_DEBUG" id="ALIGNET_DEBUG_on" value="1" ' . (Configuration::get('ALIGNET_DEBUG') == 1 ? 'checked="checked" ' : '') .'>
               <label for="ALIGNET_DEBUG_on">Sí</label>
               <input type="radio" name="ALIGNET_DEBUG" id="ALIGNET_DEBUG_off" value="0" '. (Configuration::get('ALIGNET_DEBUG') == 0 ? 'checked="checked" ' : '') . '>
               <label for="ALIGNET_DEBUG_off">No</label>
               <a class="slide-button btn"></a>
               </span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            Esquema de Integración
            </label>
            <div class="col-lg-9">
               <div class="radio ">
                  <label><input type="radio" onClick="hiddeModalCF()" name="ALIGNET_ESQUEMA" id="redirect_on" value="1" ' . (Configuration::get('ALIGNET_ESQUEMA') == 1 ? 'checked="checked" ' : '') .'>Redirect</label>
               </div>
               <div class="radio ">
                  <label><input type="radio" onClick="showModalCF()" name="ALIGNET_ESQUEMA" id="redirect_off" value="0" ' . (Configuration::get('ALIGNET_ESQUEMA') == 0 ? 'checked="checked" ' : '') .'>Modal</label>
               </div>
            </div>
         </div>
         <div class="form-group" id="ALIGNET_MODAL_DIV" ' . (Configuration::get('ALIGNET_ESQUEMA') == 1 ? 'style="display:none" ' : 'style="display:block"') .'> 
            <label class="control-label col-lg-3">
            Diseño de Modal 
            </label>
            <div class="col-lg-9">
               <div class="radio">
                  <label><input type="radio" name="ALIGNET_TIPOMODAL" id="modal_one" value="1" ' . (Configuration::get('ALIGNET_TIPOMODAL') == 1 ? 'checked="checked" ' : '') .'>Etiqueta </label>
               </div>
               <div class="radio ">
                  <label><input type="radio" name="ALIGNET_TIPOMODAL" id="modal_two" value="2" ' . (Configuration::get('ALIGNET_TIPOMODAL') == 2 ? 'checked="checked" ' : '') .'>Circulo</label>
               </div>
               <div class="radio ">
                  <label><input type="radio" name="ALIGNET_TIPOMODAL" id="modal_tre" value="3" ' . (Configuration::get('ALIGNET_TIPOMODAL') == 3 ? 'checked="checked" ' : '') .'>Rectángulo</label>
               </div>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            URL de Respuesta:
            </label>
            <div class="col-lg-9">
               <p class="help-block">
                  <b>'.$this->url_return.'</b>
               </p>
            </div>
         </div>
      </div>
      <div class="row">
   <div class="col-lg-6">
   <div class="panel" id="fieldset_1_1">
      <div class="panel-heading">
         <i class="icon-cog"></i>							CONFIGURACIÓN - MONEDA LOCAL
      </div>
      <div class="form-wrapper">
         
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            ID Adquirente
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDACQUIRER" id="ALIGNET_IDACQUIRER" value="' . Configuration::get('ALIGNET_IDACQUIRER').'" class="" required="required">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            ID Comercio
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDCOMMERCE" id="ALIGNET_IDCOMMERCE" value="' . Configuration::get('ALIGNET_IDCOMMERCE').'" class="" required="required">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            Clave V-POS2
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_KEY" id="ALIGNET_KEY" value="' . Configuration::get('ALIGNET_KEY').'" class="" required="required">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            ID Wallet
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDENTCOMMERCE" id="ALIGNET_IDENTCOMMERCE" value="' . Configuration::get('ALIGNET_IDENTCOMMERCE').'" class="" >
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            Clave Wallet
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_KEYWALLET" id="ALIGNET_KEYWALLET" value="' . Configuration::get('ALIGNET_KEYWALLET').'" class="" >
            </div>
         </div>
      </div>
      <!-- /.form-wrapper -->
   </div>
   </div>
   <div class="col-lg-6">
   <div class="panel" id="fieldset_2_2">
      <div class="panel-heading">
         <i class="icon-cog"></i>							CONFIGURACIÓN - MONEDA DÓLARES
      </div>
      <div class="form-wrapper">
        
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            ID Adquirente
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDACQUIRER_DLLS" id="ALIGNET_IDACQUIRER_DLLS" value="' . Configuration::get('ALIGNET_IDACQUIRER_DLLS').'" class="">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            ID Comercio
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDCOMMERCE_DLLS" id="ALIGNET_IDCOMMERCE_DLLS" value="' . Configuration::get('ALIGNET_IDCOMMERCE_DLLS').'" class="">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3 required">
            Clave V-POS2
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_KEY_DLLS" id="ALIGNET_KEY_DLLS" value="' . Configuration::get('ALIGNET_KEY_DLLS').'" class="">
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-lg-3">
            ID Wallet
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_IDENTCOMMERCE_DLLS" id="ALIGNET_IDENTCOMMERCE_DLLS"  value="' . Configuration::get('ALIGNET_IDENTCOMMERCE_DLLS').'" class="">
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-lg-3">
            Clave Wallet
            </label>
            <div class="col-lg-9">
               <input type="text" name="ALIGNET_KEYWALLET_DLLS" id="ALIGNET_KEYWALLET_DLLS" value="' . Configuration::get('ALIGNET_KEYWALLET_DLLS').'" class="">
            </div>
         </div>
     	 </div>
      </div>
       </div>
   </div><!-- /.form-wrapper -->
      <div class="panel-footer">
         <button type="submit" value="1" id="module_form_submit_btn" name="btnSubmitPayme" class="btn btn-default pull-right">
         <i class="process-icon-save"></i> Guardar
         </button>
      </div>
</div></form>';

			return $html;
    }


     private function postValidation()
    {
        $errors = array();
        $errorsUSD =array();

        $isAnycomplete = array();
        $isAnycompleteUSD = array();

        $this->html ='';
        if (Tools::isSubmit('btnSubmitPayme')) {
            if (!empty(Tools::getValue('ALIGNET_IDENTCOMMERCE')) && !is_numeric(Tools::getValue('ALIGNET_IDENTCOMMERCE'))) {
                $errors[] = $this->l('El campo requerido "ID Wallet" sólo permite números! : MONEDA LOCAL');
            } elseif (empty(Tools::getValue('ALIGNET_IDACQUIRER'))) {
                $errors[] = $this->l('El campo requerido "ID Adquirente" no puede estar vacio! : MONEDA LOCAL');
            } elseif (!empty(Tools::getValue('ALIGNET_IDACQUIRER')) && !is_numeric(Tools::getValue('ALIGNET_IDACQUIRER'))) {
                $errors[] = $this->l('El campo requerido "ID Adquirente" sólo permite números! : MONEDA LOCAL');
            } elseif (empty(Tools::getValue('ALIGNET_IDCOMMERCE'))) {
                $errors[] = $this->l('El campo requerido "ID Comercio" no puede estar vacio! : MONEDA LOCAL');
            } elseif (!empty(Tools::getValue('ALIGNET_IDCOMMERCE')) && !is_numeric(Tools::getValue('ALIGNET_IDCOMMERCE'))) {
                $errors[] = $this->l('El campo requerido "ID Comercio" sólo permite números! : MONEDA LOCAL');
            } elseif (empty(Tools::getValue('ALIGNET_KEY'))) {
                $errors[] = $this->l('El campo requerido "Clave V-POS2" no puede estar vacio! : MONEDA LOCAL');
            }


             if (!empty(Tools::getValue('ALIGNET_IDENTCOMMERCE_DLLS')) && !is_numeric(Tools::getValue('ALIGNET_IDENTCOMMERCE_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "ID Wallet" sólo permite números! : MONEDA DÓLARES');
            } elseif (empty(Tools::getValue('ALIGNET_IDACQUIRER_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "ID Adquirente" no puede estar vacio! : MONEDA DÓLARES');
            } elseif (!empty(Tools::getValue('ALIGNET_IDACQUIRER_DLLS')) && !is_numeric(Tools::getValue('ALIGNET_IDACQUIRER_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "ID Adquirente" sólo permite números! : MONEDA DÓLARES');
            } elseif (empty(Tools::getValue('ALIGNET_IDCOMMERCE_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "ID Comercio" no puede estar vacio! : MONEDA DÓLARES');
            } elseif (!empty(Tools::getValue('ALIGNET_IDCOMMERCE_DLLS')) && !is_numeric(Tools::getValue('ALIGNET_IDCOMMERCE_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "ID Comercio" sólo permite números! : MONEDA DÓLARES');
            } elseif (empty(Tools::getValue('ALIGNET_KEY_DLLS'))) {
                $errorsUSD[] = $this->l('El campo requerido "Clave V-POS2" no puede estar vacio! : MONEDA DÓLARES');
            }



          if (empty(Tools::getValue('ALIGNET_IDACQUIRER')) && empty(Tools::getValue('ALIGNET_IDCOMMERCE')) && empty(Tools::getValue('ALIGNET_KEYWALLET'))) {
                $isAnycomplete[] = $this->l('Falta Configurar Moneda ');
            } 


            if (empty(Tools::getValue('ALIGNET_IDACQUIRER_DLLS')) && empty(Tools::getValue('ALIGNET_IDCOMMERCE_DLLS')) && empty(Tools::getValue('ALIGNET_KEYWALLET_DLLS'))) {
                $isAnycompleteUSD[] =  $this->l('Falta Configurar Moneda ');
            } 


        }


        if (count($isAnycomplete) > 0 && count($isAnycompleteUSD) > 0) {
          $this->html .= $this->displayError("Falta Registrar Moneda");
          return false;
        }
        else {
          if (count($isAnycomplete) > 0 ) {
            if (count($errorsUSD) > 0 ) {
                $this->html .= $this->displayError(implode('<br />', $errorsUSD));
                return false;
            }   
          }
          elseif (count($isAnycompleteUSD) > 0 ) {
            if (count($errors) > 0 ) {
                $this->html .= $this->displayError(implode('<br />', $errors));
                return false;
            }   
          }
        }
        
       
        return true;
    }

    private function postProcess()
    {
    	$this->html = '';
        if (Tools::isSubmit('btnSubmitPayme')) {
            Configuration::updateValue('ALIGNET_URLTPV', Tools::getValue('ALIGNET_URLTPV'));
            Configuration::updateValue('ALIGNET_TIPOMODAL',Tools::getValue('ALIGNET_TIPOMODAL'));
            Configuration::updateValue('ALIGNET_IDACQUIRER', Tools::getValue('ALIGNET_IDACQUIRER'));
            Configuration::updateValue('ALIGNET_IDCOMMERCE', Tools::getValue('ALIGNET_IDCOMMERCE'));
            Configuration::updateValue('ALIGNET_IDENTCOMMERCE', Tools::getValue('ALIGNET_IDENTCOMMERCE'));
            Configuration::updateValue('ALIGNET_KEY', Tools::getValue('ALIGNET_KEY'));
            Configuration::updateValue('ALIGNET_KEYWALLET', Tools::getValue('ALIGNET_KEYWALLET'));
            Configuration::updateValue('ALIGNET_IDCOMMERCE_DLLS', Tools::getValue('ALIGNET_IDCOMMERCE_DLLS'));
            Configuration::updateValue('ALIGNET_IDACQUIRER_DLLS', Tools::getValue('ALIGNET_IDACQUIRER_DLLS'));
            Configuration::updateValue('ALIGNET_IDENTCOMMERCE_DLLS', Tools::getValue('ALIGNET_IDENTCOMMERCE_DLLS'));
            Configuration::updateValue('ALIGNET_KEY_DLLS', Tools::getValue('ALIGNET_KEY_DLLS'));
            Configuration::updateValue('ALIGNET_KEYWALLET_DLLS', Tools::getValue('ALIGNET_KEYWALLET_DLLS'));
            Configuration::updateValue('ALIGNET_DEBUG', Tools::getValue('ALIGNET_DEBUG'));
            Configuration::updateValue('ALIGNET_ESQUEMA', Tools::getValue('ALIGNET_ESQUEMA'));
            if (_PS_VERSION_ < '1.6') {
                $this->_html .= '<p>'.$this->l('Configuración guardada correctamente.').'</p>';
            }

            $this->html .= $this->displayConfirmation($this->l('Configuración Guardado Correctamente'));
        }
        
    }

    protected function displayConfiguration()
    {
        return $this->display(__FILE__, 'views/templates/admin/configuration.tpl');
    }

    public function getParameteresCommerce()
    {
        $cart = $this->context->cart;
        $customer = $this->context->customer;
        
        $currency = new Currency($cart->id_currency);
        $purchaseCurrencyCode = $currency->iso_code_num;
        $billing_address = new Address(Context::getContext()->cart->id_address_invoice);
        $billing_address->country = new Country($billing_address->id_country);
        $delivery_address = new Address(Context::getContext()->cart->id_address_delivery);
        $delivery_address->country = new Country($delivery_address->id_country);
        
        if ($purchaseCurrencyCode == 840) {
            $this->idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE_DLLS');
            $this->keywallet = Configuration::get('ALIGNET_KEYWALLET_DLLS');
            $this->acquirerId = Configuration::get('ALIGNET_IDACQUIRER_DLLS');
            $this->idCommerce = Configuration::get('ALIGNET_IDCOMMERCE_DLLS');
            $this->key = Configuration::get('ALIGNET_KEY_DLLS');
        } else {
            $this->idEntCommerce = Configuration::get('ALIGNET_IDENTCOMMERCE');
            $this->keywallet = Configuration::get('ALIGNET_KEYWALLET');
            $this->acquirerId = Configuration::get('ALIGNET_IDACQUIRER');
            $this->idCommerce = Configuration::get('ALIGNET_IDCOMMERCE');
            $this->key = Configuration::get('ALIGNET_KEY');
        }

        $long = ($this->acquirerId == 144 || $this->acquirerId == 29) ? 6 : 9;
        
        $commerce = array();
        $commerce['reserved1'] ='';
        $commerce['reserved2'] ='';
        $commerce['reserved3'] ='';
        $commerce['reserved4'] ='';
        $commerce['reserved5'] = '';
        $commerce['reserved6'] ='';
        $commerce['reserved7'] ='';
        $commerce['reserved8'] ='';
        $commerce['reserved9'] ='';
        $commerce['reserved10'] = '';
        $commerce['purchaseOperationNumber'] = str_pad($cart->id, $long, "0", STR_PAD_LEFT);
        $commerce['purchaseAmount'] = str_replace('.', '', number_format($cart->getOrderTotal(true, 3), 2, '.', ''));
        $commerce['purchaseCurrencyCode'] = $purchaseCurrencyCode;
        $commerce['language'] = ($this->context->language->iso_code == 'en') ? 'EN' : 'ES' ;
        $commerce['billingFirstName'] = $billing_address->firstname;
        $commerce['billingLastName'] = $billing_address->lastname;
        $commerce['billingEmail'] = $customer->email;
        $commerce['billingAddress'] = $billing_address->address1;
        $commerce['billingZip'] = ($billing_address->postcode == "") ? "Postcode 01" : $billing_address->postcode;
        $commerce['billingCity'] = $billing_address->city;
        $commerce['billingState'] = $billing_address->city;
        $commerce['billingCountry'] = $billing_address->country->iso_code;
        $commerce['billingPhone'] = $billing_address->phone;
        $commerce['shippingFirstName'] = $delivery_address->firstname;
        $commerce['shippingLastName'] = $delivery_address->lastname;
        $commerce['shippingEmail'] = $customer->email;
        $commerce['shippingAddress'] = $delivery_address->address1;
        $commerce['shippingZip'] = ($delivery_address->postcode == "") ? "Postcode 01" : $delivery_address->postcode;
        $commerce['shippingCity'] = $delivery_address->city;
        $commerce['shippingState'] = $billing_address->city;
        $commerce['shippingCountry'] = $delivery_address->country->iso_code;
        $commerce['shippingPhone'] = $delivery_address->phone;
        $commerce['programmingLanguage'] = 'ALG-PS-v'.$this->version;
        $commerce['userCommerce'] = $customer->id;
        $commerce['userCodePayme'] = $this->userCodePayme($commerce);
        $commerce['descriptionProducts'] =
            $this->cleanText(count($cart->getProducts()) > 1 ? 'Productos Varios' : $cart->getProducts()[0]['name']) ;
        $commerce['purchaseVerification'] =
            $this->purchaseVerification($commerce['purchaseOperationNumber'], $commerce['purchaseAmount'], $commerce['purchaseCurrencyCode']);

        return $commerce;
    }


    public function cleanText($cad)
    {
        $cadStr= array (
            "á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™",
            "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ",
            "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹"
        );
            
        $cadPermit= array (
            "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U",
            "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U",
            "u", "o", "O", "i", "a", "e", "U", "I", "A", "E"
        );
        $value = str_replace($cadStr, $cadPermit, $cad);
        return $value;
    }

    public function addOrderState($name)
    {
        $state_exist = false;
        $states = OrderState::getOrderStates((int)$this->context->language->id);
 
     
        foreach ($states as $state) {
            if (in_array($name, $state)) {
                $state_exist = true;
                break;
            }
        }
 
        if (!$state_exist) {
 
            $order_state = new OrderState();
            $order_state->color = '#E8840C';
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->name = array();
            $languages = Language::getLanguages(false);
            foreach ($languages as $language)
                $order_state->name[ $language['id_lang'] ] = 'Pendiente Pago';
 
            if ($order_state->add()) {
						$source = dirname(__FILE__) . '/logo.gif';
						$destination = dirname(__FILE__) . '/../../img/os/' . (int) $order_state->id . '.gif';
						copy($source, $destination);
				}
            Configuration::updateValue($name, (int) $order_state->id);
        }
 
        return true;
    }

    public function userCodePayme($params)
    {
        
        $concatRegister = $this->idEntCommerce.$params['userCommerce'].$params['billingEmail'].$this->keywallet;
        $registerVerification =
            (phpversion() >= 5.3) ? openssl_digest($concatRegister, 'sha512') : hash('sha512', $concatRegister);
    
        $paramsWallet = array(
            'idEntCommerce' => (string)$this->idEntCommerce,
            'codCardHolderCommerce' => (string)$params['userCommerce'],
            'names' => $params['billingFirstName'],
            'lastNames' => $params['billingLastName'],
            'mail' => $params['billingEmail'] ,
            'reserved1' => $params['reserved1'],
            'reserved2' => $params['reserved2'],
            'reserved3' => $params['reserved3'],
            'registerVerification'=>$registerVerification
        );
        $codAsoCardHolder = "";

        try {

        	$clientWallet = new SoapClient($this->wsdl);
        	$resultWallet = $clientWallet->RegisterCardHolder($paramsWallet);
        	$codAsoCardHolder = $resultWallet->codAsoCardHolderWallet;
        	
        } catch (Exception $e) {
        	
        }
      
        
        return  $codAsoCardHolder;
    }
    
    public function purchaseVerification($purchOperNum, $purchAmo, $purchCurrCod, $authRes = null)
    {
        $concatPurchase = $this->acquirerId.$this->idCommerce.$purchOperNum.$purchAmo.$purchCurrCod.$authRes.$this->key;
        
        return (phpversion() >= 5.3) ? openssl_digest($concatPurchase, 'sha512') : hash('sha512', $concatPurchase);
    }


     /**
     * return correct path for .tpl file
     *
     * @param $area
     * @param $file
     * @return string
     */
    public function getTemplate($area, $file)
    {
        return 'module:payme/views/templates/'.$area.'/'.$file;
    }
}
?>
